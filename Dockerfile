# Start from Node 20 Alpine base image
FROM node:20-alpine

# Set environment variables for Chrome
ENV CHROME_BIN=/usr/bin/chromium-browser \
    CHROME_PATH=/usr/lib/chromium/

# Update and install packages
RUN apk update && apk add --no-cache \
    chromium-swiftshader \
    ttf-freefont \
    font-noto-emoji \
    git \
    tini \
    && apk add --no-cache \
    --repository=https://dl-cdn.alpinelinux.org/alpine/edge/testing \
    font-wqy-zenhei

# install http server
RUN npm install -g http-server

RUN mkdir -p /usr/src/app 

COPY run-chromium-and-http-server.sh /usr/src/app/.

# Run Chrome as non-privileged
WORKDIR /usr/src/app

ENTRYPOINT [ "/sbin/tini", "--" ]
CMD [ "./run-chromium-and-http-server.sh" ]
