# alpine-chrome-with-node-image-http-server 

Chrome headless docker images

Available on [docker hub](https://hub.docker.com/repository/docker/alistaireverett/alpine-chrome-with-node-image-http-server)

## update instructions

1. navigate to folder with Dockerfile
1. update the Dockerfile contents if needed
1. update the tagname if needed
1. run these commands to create a multi-arch image:

```
docker login

docker buildx create --use

docker buildx build --platform=linux/amd64,linux/arm64 --push -t  alistaireverett/alpine-chrome-with-node-image-http-server .

```
