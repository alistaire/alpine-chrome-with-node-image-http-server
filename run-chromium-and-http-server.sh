#!/bin/sh

# Start the first process
chromium-browser --headless --use-gl=swiftshader --disable-software-rasterizer --disable-dev-shm-usage --no-sandbox --hide-scrollbars --remote-debugging-port=9222 --remote-debugging-address=0.0.0.0 &

# Start the second process
npx http-server /usr/src/dist/storybook/ --port 6006 --silent &

# Wait for any process to exit
wait -n

# Exit with status of process that exited first
exit $?
